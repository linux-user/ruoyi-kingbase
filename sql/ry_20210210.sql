--
-- Kingbase database dump
--

-- Dumped from database version V008R003C002B0160
-- Dumped by sys_dump version V008R003C002B0160

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT sys_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: gen_table; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."gen_table" (
    "table_id" BIGINT NOT NULL,
    "table_name" CHARACTER VARYING(200 char) DEFAULT NULL::CHARACTER VARYING,
    "table_comment" CHARACTER VARYING(500 char) DEFAULT NULL::CHARACTER VARYING,
    "sub_table_name" CHARACTER VARYING(64 char),
    "sub_table_fk_name" CHARACTER VARYING(64 char),
    "class_name" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "tpl_category" CHARACTER VARYING(200 char) DEFAULT 'crud'::CHARACTER VARYING,
    "package_name" CHARACTER VARYING(100 char),
    "module_name" CHARACTER VARYING(30 char),
    "business_name" CHARACTER VARYING(30 char),
    "function_name" CHARACTER VARYING(50 char),
    "function_author" CHARACTER VARYING(50 char),
    "gen_type" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "gen_path" CHARACTER VARYING(200 char) DEFAULT '/'::CHARACTER VARYING,
    "options" CHARACTER VARYING(1000 char),
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."gen_table" OWNER TO "SYSTEM";

--
-- Name: TABLE "gen_table"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."gen_table" IS '代码生成业务表';


--
-- Name: COLUMN "gen_table"."table_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."table_id" IS '编号';


--
-- Name: COLUMN "gen_table"."table_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."table_name" IS '表名称';


--
-- Name: COLUMN "gen_table"."table_comment"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."table_comment" IS '表描述';


--
-- Name: COLUMN "gen_table"."sub_table_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."sub_table_name" IS '关联子表的表名';


--
-- Name: COLUMN "gen_table"."sub_table_fk_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."sub_table_fk_name" IS '子表关联的外键名';


--
-- Name: COLUMN "gen_table"."class_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."class_name" IS '实体类名称';


--
-- Name: COLUMN "gen_table"."tpl_category"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."tpl_category" IS '使用的模板（crud单表操作 tree树表操作 sub主子表操作）';


--
-- Name: COLUMN "gen_table"."package_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."package_name" IS '生成包路径';


--
-- Name: COLUMN "gen_table"."module_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."module_name" IS '生成模块名';


--
-- Name: COLUMN "gen_table"."business_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."business_name" IS '生成业务名';


--
-- Name: COLUMN "gen_table"."function_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."function_name" IS '生成功能名';


--
-- Name: COLUMN "gen_table"."function_author"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."function_author" IS '生成功能作者';


--
-- Name: COLUMN "gen_table"."gen_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."gen_type" IS '生成代码方式（0zip压缩包 1自定义路径）';


--
-- Name: COLUMN "gen_table"."gen_path"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."gen_path" IS '生成路径（不填默认项目路径）';


--
-- Name: COLUMN "gen_table"."options"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."options" IS '其它生成选项';


--
-- Name: COLUMN "gen_table"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."create_by" IS '创建者';


--
-- Name: COLUMN "gen_table"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."create_time" IS '创建时间';


--
-- Name: COLUMN "gen_table"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."update_by" IS '更新者';


--
-- Name: COLUMN "gen_table"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."update_time" IS '更新时间';


--
-- Name: COLUMN "gen_table"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table"."remark" IS '备注';


--
-- Name: gen_table_column; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."gen_table_column" (
    "column_id" BIGINT NOT NULL,
    "table_id" CHARACTER VARYING(64 char),
    "column_name" CHARACTER VARYING(200 char),
    "column_comment" CHARACTER VARYING(500 char),
    "column_type" CHARACTER VARYING(100 char),
    "java_type" CHARACTER VARYING(500 char),
    "java_field" CHARACTER VARYING(200 char),
    "is_pk" CHARACTER(1 char),
    "is_increment" CHARACTER(1 char),
    "is_required" CHARACTER(1 char),
    "is_insert" CHARACTER(1 char),
    "is_edit" CHARACTER(1 char),
    "is_list" CHARACTER(1 char),
    "is_query" CHARACTER(1 char),
    "query_type" CHARACTER VARYING(200 char) DEFAULT 'EQ'::CHARACTER VARYING,
    "html_type" CHARACTER VARYING(200 char),
    "dict_type" CHARACTER VARYING(200 char) DEFAULT NULL::CHARACTER VARYING,
    "sort" INTEGER,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE
);


ALTER TABLE "PUBLIC"."gen_table_column" OWNER TO "SYSTEM";

--
-- Name: TABLE "gen_table_column"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."gen_table_column" IS '代码生成业务表字段';


--
-- Name: COLUMN "gen_table_column"."column_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."column_id" IS '编号';


--
-- Name: COLUMN "gen_table_column"."table_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."table_id" IS '归属表编号';


--
-- Name: COLUMN "gen_table_column"."column_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."column_name" IS '列名称';


--
-- Name: COLUMN "gen_table_column"."column_comment"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."column_comment" IS '列描述';


--
-- Name: COLUMN "gen_table_column"."column_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."column_type" IS '列类型';


--
-- Name: COLUMN "gen_table_column"."java_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."java_type" IS 'JAVA类型';


--
-- Name: COLUMN "gen_table_column"."java_field"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."java_field" IS 'JAVA字段名';


--
-- Name: COLUMN "gen_table_column"."is_pk"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_pk" IS '是否主键（1是）';


--
-- Name: COLUMN "gen_table_column"."is_increment"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_increment" IS '是否自增（1是）';


--
-- Name: COLUMN "gen_table_column"."is_required"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_required" IS '是否必填（1是）';


--
-- Name: COLUMN "gen_table_column"."is_insert"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_insert" IS '是否为插入字段（1是）';


--
-- Name: COLUMN "gen_table_column"."is_edit"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_edit" IS '是否编辑字段（1是）';


--
-- Name: COLUMN "gen_table_column"."is_list"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_list" IS '是否列表字段（1是）';


--
-- Name: COLUMN "gen_table_column"."is_query"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."is_query" IS '是否查询字段（1是）';


--
-- Name: COLUMN "gen_table_column"."query_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."query_type" IS '查询方式（等于、不等于、大于、小于、范围）';


--
-- Name: COLUMN "gen_table_column"."html_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."html_type" IS '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）';


--
-- Name: COLUMN "gen_table_column"."dict_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."dict_type" IS '字典类型';


--
-- Name: COLUMN "gen_table_column"."sort"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."sort" IS '排序';


--
-- Name: COLUMN "gen_table_column"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."create_by" IS '创建者';


--
-- Name: COLUMN "gen_table_column"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."create_time" IS '创建时间';


--
-- Name: COLUMN "gen_table_column"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."update_by" IS '更新者';


--
-- Name: COLUMN "gen_table_column"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."gen_table_column"."update_time" IS '更新时间';


--
-- Name: gen_table_column_column_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."gen_table_column_column_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."gen_table_column_column_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: gen_table_column_column_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."gen_table_column_column_id_SEQ" OWNED BY "PUBLIC"."gen_table_column"."column_id";


--
-- Name: gen_table_table_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."gen_table_table_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."gen_table_table_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: gen_table_table_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."gen_table_table_id_SEQ" OWNED BY "PUBLIC"."gen_table"."table_id";


--
-- Name: sys_config; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_config" (
    "config_id" INTEGER NOT NULL,
    "config_name" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "config_key" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "config_value" CHARACTER VARYING(500 char) DEFAULT NULL::CHARACTER VARYING,
    "config_type" CHARACTER(1 char) DEFAULT 'N'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."sys_config" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_config"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_config" IS '参数配置表';


--
-- Name: COLUMN "sys_config"."config_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."config_id" IS '参数主键';


--
-- Name: COLUMN "sys_config"."config_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."config_name" IS '参数名称';


--
-- Name: COLUMN "sys_config"."config_key"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."config_key" IS '参数键名';


--
-- Name: COLUMN "sys_config"."config_value"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."config_value" IS '参数键值';


--
-- Name: COLUMN "sys_config"."config_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."config_type" IS '系统内置（Y是 N否）';


--
-- Name: COLUMN "sys_config"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_config"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_config"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_config"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_config"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_config"."remark" IS '备注';


--
-- Name: sys_config_config_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_config_config_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_config_config_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_config_config_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_config_config_id_SEQ" OWNED BY "PUBLIC"."sys_config"."config_id";


--
-- Name: sys_dept; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_dept" (
    "dept_id" BIGINT NOT NULL,
    "parent_id" BIGINT DEFAULT 0,
    "ancestors" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "dept_name" CHARACTER VARYING(30 char) DEFAULT NULL::CHARACTER VARYING,
    "order_num" INTEGER DEFAULT 0,
    "leader" CHARACTER VARYING(20 char),
    "phone" CHARACTER VARYING(11 char),
    "email" CHARACTER VARYING(50 char),
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "del_flag" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE
);


ALTER TABLE "PUBLIC"."sys_dept" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_dept"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_dept" IS '部门表';


--
-- Name: COLUMN "sys_dept"."dept_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."dept_id" IS '部门id';


--
-- Name: COLUMN "sys_dept"."parent_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."parent_id" IS '父部门id';


--
-- Name: COLUMN "sys_dept"."ancestors"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."ancestors" IS '祖级列表';


--
-- Name: COLUMN "sys_dept"."dept_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."dept_name" IS '部门名称';


--
-- Name: COLUMN "sys_dept"."order_num"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."order_num" IS '显示顺序';


--
-- Name: COLUMN "sys_dept"."leader"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."leader" IS '负责人';


--
-- Name: COLUMN "sys_dept"."phone"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."phone" IS '联系电话';


--
-- Name: COLUMN "sys_dept"."email"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."email" IS '邮箱';


--
-- Name: COLUMN "sys_dept"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."status" IS '部门状态（0正常 1停用）';


--
-- Name: COLUMN "sys_dept"."del_flag"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."del_flag" IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN "sys_dept"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_dept"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_dept"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_dept"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dept"."update_time" IS '更新时间';


--
-- Name: sys_dept_dept_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_dept_dept_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_dept_dept_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_dept_dept_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_dept_dept_id_SEQ" OWNED BY "PUBLIC"."sys_dept"."dept_id";


--
-- Name: sys_dict_data; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_dict_data" (
    "dict_code" BIGINT NOT NULL,
    "dict_sort" INTEGER DEFAULT 0,
    "dict_label" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "dict_value" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "dict_type" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "css_class" CHARACTER VARYING(100 char),
    "list_class" CHARACTER VARYING(100 char),
    "is_default" CHARACTER(1 char) DEFAULT 'N'::BPCHAR,
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."sys_dict_data" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_dict_data"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_dict_data" IS '字典数据表';


--
-- Name: COLUMN "sys_dict_data"."dict_code"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."dict_code" IS '字典编码';


--
-- Name: COLUMN "sys_dict_data"."dict_sort"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."dict_sort" IS '字典排序';


--
-- Name: COLUMN "sys_dict_data"."dict_label"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."dict_label" IS '字典标签';


--
-- Name: COLUMN "sys_dict_data"."dict_value"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."dict_value" IS '字典键值';


--
-- Name: COLUMN "sys_dict_data"."dict_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."dict_type" IS '字典类型';


--
-- Name: COLUMN "sys_dict_data"."css_class"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."css_class" IS '样式属性（其他样式扩展）';


--
-- Name: COLUMN "sys_dict_data"."list_class"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."list_class" IS '表格回显样式';


--
-- Name: COLUMN "sys_dict_data"."is_default"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."is_default" IS '是否默认（Y是 N否）';


--
-- Name: COLUMN "sys_dict_data"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."status" IS '状态（0正常 1停用）';


--
-- Name: COLUMN "sys_dict_data"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_dict_data"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_dict_data"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_dict_data"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_dict_data"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_data"."remark" IS '备注';


--
-- Name: sys_dict_data_dict_code_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_dict_data_dict_code_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_dict_data_dict_code_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_dict_data_dict_code_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_dict_data_dict_code_SEQ" OWNED BY "PUBLIC"."sys_dict_data"."dict_code";


--
-- Name: sys_dict_type; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_dict_type" (
    "dict_id" BIGINT NOT NULL,
    "dict_name" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "dict_type" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."sys_dict_type" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_dict_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_dict_type" IS '字典类型表';


--
-- Name: COLUMN "sys_dict_type"."dict_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."dict_id" IS '字典主键';


--
-- Name: COLUMN "sys_dict_type"."dict_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."dict_name" IS '字典名称';


--
-- Name: COLUMN "sys_dict_type"."dict_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."dict_type" IS '字典类型';


--
-- Name: COLUMN "sys_dict_type"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."status" IS '状态（0正常 1停用）';


--
-- Name: COLUMN "sys_dict_type"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_dict_type"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_dict_type"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_dict_type"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_dict_type"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_dict_type"."remark" IS '备注';


--
-- Name: sys_dict_type_dict_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_dict_type_dict_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_dict_type_dict_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_dict_type_dict_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_dict_type_dict_id_SEQ" OWNED BY "PUBLIC"."sys_dict_type"."dict_id";


--
-- Name: sys_job; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_job" (
    "job_id" BIGINT NOT NULL,
    "job_name" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING NOT NULL,
    "job_group" CHARACTER VARYING(64 char) DEFAULT 'DEFAULT'::CHARACTER VARYING NOT NULL,
    "invoke_target" CHARACTER VARYING(500 char) NOT NULL,
    "cron_expression" CHARACTER VARYING(255 char) DEFAULT NULL::CHARACTER VARYING,
    "misfire_policy" CHARACTER VARYING(20 char) DEFAULT '3'::CHARACTER VARYING,
    "concurrent" CHARACTER(1 char) DEFAULT '1'::BPCHAR,
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char) DEFAULT NULL::CHARACTER VARYING
);


ALTER TABLE "PUBLIC"."sys_job" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_job"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_job" IS '定时任务调度表';


--
-- Name: COLUMN "sys_job"."job_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."job_id" IS '任务ID';


--
-- Name: COLUMN "sys_job"."job_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."job_name" IS '任务名称';


--
-- Name: COLUMN "sys_job"."job_group"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."job_group" IS '任务组名';


--
-- Name: COLUMN "sys_job"."invoke_target"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."invoke_target" IS '调用目标字符串';


--
-- Name: COLUMN "sys_job"."cron_expression"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."cron_expression" IS 'cron执行表达式';


--
-- Name: COLUMN "sys_job"."misfire_policy"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."misfire_policy" IS '计划执行错误策略（1立即执行 2执行一次 3放弃执行）';


--
-- Name: COLUMN "sys_job"."concurrent"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."concurrent" IS '是否并发执行（0允许 1禁止）';


--
-- Name: COLUMN "sys_job"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."status" IS '状态（0正常 1暂停）';


--
-- Name: COLUMN "sys_job"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_job"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_job"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_job"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_job"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job"."remark" IS '备注信息';


--
-- Name: sys_job_job_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_job_job_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_job_job_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_job_job_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_job_job_id_SEQ" OWNED BY "PUBLIC"."sys_job"."job_id";


--
-- Name: sys_job_log; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_job_log" (
    "job_log_id" BIGINT NOT NULL,
    "job_name" CHARACTER VARYING(64 char) NOT NULL,
    "job_group" CHARACTER VARYING(64 char) NOT NULL,
    "invoke_target" CHARACTER VARYING(500 char) NOT NULL,
    "job_message" CHARACTER VARYING(500 char),
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "exception_info" CHARACTER VARYING(2000 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE
);


ALTER TABLE "PUBLIC"."sys_job_log" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_job_log"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_job_log" IS '定时任务调度日志表';


--
-- Name: COLUMN "sys_job_log"."job_log_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."job_log_id" IS '任务日志ID';


--
-- Name: COLUMN "sys_job_log"."job_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."job_name" IS '任务名称';


--
-- Name: COLUMN "sys_job_log"."job_group"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."job_group" IS '任务组名';


--
-- Name: COLUMN "sys_job_log"."invoke_target"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."invoke_target" IS '调用目标字符串';


--
-- Name: COLUMN "sys_job_log"."job_message"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."job_message" IS '日志信息';


--
-- Name: COLUMN "sys_job_log"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."status" IS '执行状态（0正常 1失败）';


--
-- Name: COLUMN "sys_job_log"."exception_info"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."exception_info" IS '异常信息';


--
-- Name: COLUMN "sys_job_log"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_job_log"."create_time" IS '创建时间';


--
-- Name: sys_job_log_job_log_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_job_log_job_log_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_job_log_job_log_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_job_log_job_log_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_job_log_job_log_id_SEQ" OWNED BY "PUBLIC"."sys_job_log"."job_log_id";


--
-- Name: sys_logininfor; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_logininfor" (
    "info_id" BIGINT NOT NULL,
    "login_name" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "ipaddr" CHARACTER VARYING(128 char) DEFAULT NULL::CHARACTER VARYING,
    "login_location" CHARACTER VARYING(255 char) DEFAULT NULL::CHARACTER VARYING,
    "browser" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "os" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "msg" CHARACTER VARYING(255 char) DEFAULT NULL::CHARACTER VARYING,
    "login_time" TIMESTAMP WITHOUT TIME ZONE
);


ALTER TABLE "PUBLIC"."sys_logininfor" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_logininfor"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_logininfor" IS '系统访问记录';


--
-- Name: COLUMN "sys_logininfor"."info_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."info_id" IS '访问ID';


--
-- Name: COLUMN "sys_logininfor"."login_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."login_name" IS '登录账号';


--
-- Name: COLUMN "sys_logininfor"."ipaddr"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."ipaddr" IS '登录IP地址';


--
-- Name: COLUMN "sys_logininfor"."login_location"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."login_location" IS '登录地点';


--
-- Name: COLUMN "sys_logininfor"."browser"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."browser" IS '浏览器类型';


--
-- Name: COLUMN "sys_logininfor"."os"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."os" IS '操作系统';


--
-- Name: COLUMN "sys_logininfor"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."status" IS '登录状态（0成功 1失败）';


--
-- Name: COLUMN "sys_logininfor"."msg"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."msg" IS '提示消息';


--
-- Name: COLUMN "sys_logininfor"."login_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_logininfor"."login_time" IS '访问时间';


--
-- Name: sys_logininfor_info_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_logininfor_info_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_logininfor_info_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_logininfor_info_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_logininfor_info_id_SEQ" OWNED BY "PUBLIC"."sys_logininfor"."info_id";


--
-- Name: sys_menu; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_menu" (
    "menu_id" BIGINT NOT NULL,
    "menu_name" CHARACTER VARYING(50 char) NOT NULL,
    "parent_id" BIGINT DEFAULT 0,
    "order_num" INTEGER DEFAULT 0,
    "url" CHARACTER VARYING(200 char) DEFAULT '#'::CHARACTER VARYING,
    "target" CHARACTER VARYING(20 char) DEFAULT NULL::CHARACTER VARYING,
    "menu_type" CHARACTER(1 char) DEFAULT NULL::BPCHAR,
    "visible" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "is_refresh" CHARACTER(1 char) DEFAULT '1'::BPCHAR,
    "perms" CHARACTER VARYING(100 char),
    "icon" CHARACTER VARYING(100 char) DEFAULT '#'::CHARACTER VARYING,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char) DEFAULT NULL::CHARACTER VARYING
);


ALTER TABLE "PUBLIC"."sys_menu" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_menu"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_menu" IS '菜单权限表';


--
-- Name: COLUMN "sys_menu"."menu_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."menu_id" IS '菜单ID';


--
-- Name: COLUMN "sys_menu"."menu_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."menu_name" IS '菜单名称';


--
-- Name: COLUMN "sys_menu"."parent_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."parent_id" IS '父菜单ID';


--
-- Name: COLUMN "sys_menu"."order_num"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."order_num" IS '显示顺序';


--
-- Name: COLUMN "sys_menu"."url"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."url" IS '请求地址';


--
-- Name: COLUMN "sys_menu"."target"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."target" IS '打开方式（menuItem页签 menuBlank新窗口）';


--
-- Name: COLUMN "sys_menu"."menu_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."menu_type" IS '菜单类型（M目录 C菜单 F按钮）';


--
-- Name: COLUMN "sys_menu"."visible"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."visible" IS '菜单状态（0显示 1隐藏）';


--
-- Name: COLUMN "sys_menu"."is_refresh"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."is_refresh" IS '是否刷新（0刷新 1不刷新）';


--
-- Name: COLUMN "sys_menu"."perms"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."perms" IS '权限标识';


--
-- Name: COLUMN "sys_menu"."icon"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."icon" IS '菜单图标';


--
-- Name: COLUMN "sys_menu"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_menu"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_menu"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_menu"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_menu"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_menu"."remark" IS '备注';


--
-- Name: sys_menu_menu_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_menu_menu_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_menu_menu_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_menu_menu_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_menu_menu_id_SEQ" OWNED BY "PUBLIC"."sys_menu"."menu_id";


--
-- Name: sys_notice; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_notice" (
    "notice_id" INTEGER NOT NULL,
    "notice_title" CHARACTER VARYING(50 char) NOT NULL,
    "notice_type" CHARACTER(1 char) NOT NULL,
    "notice_content" CHARACTER VARYING(2000 char),
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(255 char)
);


ALTER TABLE "PUBLIC"."sys_notice" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_notice"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_notice" IS '通知公告表';


--
-- Name: COLUMN "sys_notice"."notice_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."notice_id" IS '公告ID';


--
-- Name: COLUMN "sys_notice"."notice_title"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."notice_title" IS '公告标题';


--
-- Name: COLUMN "sys_notice"."notice_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."notice_type" IS '公告类型（1通知 2公告）';


--
-- Name: COLUMN "sys_notice"."notice_content"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."notice_content" IS '公告内容';


--
-- Name: COLUMN "sys_notice"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."status" IS '公告状态（0正常 1关闭）';


--
-- Name: COLUMN "sys_notice"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_notice"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_notice"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_notice"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_notice"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_notice"."remark" IS '备注';


--
-- Name: sys_notice_notice_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_notice_notice_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_notice_notice_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_notice_notice_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_notice_notice_id_SEQ" OWNED BY "PUBLIC"."sys_notice"."notice_id";


--
-- Name: sys_oper_log; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_oper_log" (
    "oper_id" BIGINT NOT NULL,
    "title" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "business_type" INTEGER DEFAULT 0,
    "method" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "request_method" CHARACTER VARYING(10 char) DEFAULT NULL::CHARACTER VARYING,
    "operator_type" INTEGER DEFAULT 0,
    "oper_name" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "dept_name" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "oper_url" CHARACTER VARYING(255 char) DEFAULT NULL::CHARACTER VARYING,
    "oper_ip" CHARACTER VARYING(128 char) DEFAULT NULL::CHARACTER VARYING,
    "oper_location" CHARACTER VARYING(255 char) DEFAULT NULL::CHARACTER VARYING,
    "oper_param" CHARACTER VARYING(2000 char) DEFAULT NULL::CHARACTER VARYING,
    "json_result" CHARACTER VARYING(2000 char) DEFAULT NULL::CHARACTER VARYING,
    "status" INTEGER DEFAULT 0,
    "error_msg" CHARACTER VARYING(2000 char) DEFAULT NULL::CHARACTER VARYING,
    "oper_time" TIMESTAMP WITHOUT TIME ZONE
);


ALTER TABLE "PUBLIC"."sys_oper_log" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_oper_log"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_oper_log" IS '操作日志记录';


--
-- Name: COLUMN "sys_oper_log"."oper_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_id" IS '日志主键';


--
-- Name: COLUMN "sys_oper_log"."title"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."title" IS '模块标题';


--
-- Name: COLUMN "sys_oper_log"."business_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."business_type" IS '业务类型（0其它 1新增 2修改 3删除）';


--
-- Name: COLUMN "sys_oper_log"."method"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."method" IS '方法名称';


--
-- Name: COLUMN "sys_oper_log"."request_method"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."request_method" IS '请求方式';


--
-- Name: COLUMN "sys_oper_log"."operator_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."operator_type" IS '操作类别（0其它 1后台用户 2手机端用户）';


--
-- Name: COLUMN "sys_oper_log"."oper_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_name" IS '操作人员';


--
-- Name: COLUMN "sys_oper_log"."dept_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."dept_name" IS '部门名称';


--
-- Name: COLUMN "sys_oper_log"."oper_url"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_url" IS '请求URL';


--
-- Name: COLUMN "sys_oper_log"."oper_ip"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_ip" IS '主机地址';


--
-- Name: COLUMN "sys_oper_log"."oper_location"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_location" IS '操作地点';


--
-- Name: COLUMN "sys_oper_log"."oper_param"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_param" IS '请求参数';


--
-- Name: COLUMN "sys_oper_log"."json_result"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."json_result" IS '返回参数';


--
-- Name: COLUMN "sys_oper_log"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."status" IS '操作状态（0正常 1异常）';


--
-- Name: COLUMN "sys_oper_log"."error_msg"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."error_msg" IS '错误消息';


--
-- Name: COLUMN "sys_oper_log"."oper_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_oper_log"."oper_time" IS '操作时间';


--
-- Name: sys_oper_log_oper_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_oper_log_oper_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_oper_log_oper_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_oper_log_oper_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_oper_log_oper_id_SEQ" OWNED BY "PUBLIC"."sys_oper_log"."oper_id";


--
-- Name: sys_post; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_post" (
    "post_id" BIGINT NOT NULL,
    "post_code" CHARACTER VARYING(64 char) NOT NULL,
    "post_name" CHARACTER VARYING(50 char) NOT NULL,
    "post_sort" INTEGER NOT NULL,
    "status" CHARACTER(1 char) NOT NULL,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."sys_post" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_post"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_post" IS '岗位信息表';


--
-- Name: COLUMN "sys_post"."post_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."post_id" IS '岗位ID';


--
-- Name: COLUMN "sys_post"."post_code"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."post_code" IS '岗位编码';


--
-- Name: COLUMN "sys_post"."post_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."post_name" IS '岗位名称';


--
-- Name: COLUMN "sys_post"."post_sort"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."post_sort" IS '显示顺序';


--
-- Name: COLUMN "sys_post"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."status" IS '状态（0正常 1停用）';


--
-- Name: COLUMN "sys_post"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_post"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_post"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_post"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_post"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_post"."remark" IS '备注';


--
-- Name: sys_post_post_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_post_post_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_post_post_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_post_post_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_post_post_id_SEQ" OWNED BY "PUBLIC"."sys_post"."post_id";


--
-- Name: sys_role; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_role" (
    "role_id" BIGINT NOT NULL,
    "role_name" CHARACTER VARYING(30 char) NOT NULL,
    "role_key" CHARACTER VARYING(100 char) NOT NULL,
    "role_sort" INTEGER NOT NULL,
    "data_scope" CHARACTER(1 char) DEFAULT '1'::BPCHAR,
    "status" CHARACTER(1 char) NOT NULL,
    "del_flag" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."sys_role" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_role"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_role" IS '角色信息表';


--
-- Name: COLUMN "sys_role"."role_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."role_id" IS '角色ID';


--
-- Name: COLUMN "sys_role"."role_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."role_name" IS '角色名称';


--
-- Name: COLUMN "sys_role"."role_key"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."role_key" IS '角色权限字符串';


--
-- Name: COLUMN "sys_role"."role_sort"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."role_sort" IS '显示顺序';


--
-- Name: COLUMN "sys_role"."data_scope"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."data_scope" IS '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）';


--
-- Name: COLUMN "sys_role"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."status" IS '角色状态（0正常 1停用）';


--
-- Name: COLUMN "sys_role"."del_flag"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."del_flag" IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN "sys_role"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_role"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_role"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_role"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_role"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role"."remark" IS '备注';


--
-- Name: sys_role_dept; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_role_dept" (
    "role_id" BIGINT NOT NULL,
    "dept_id" BIGINT NOT NULL
);


ALTER TABLE "PUBLIC"."sys_role_dept" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_role_dept"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_role_dept" IS '角色和部门关联表';


--
-- Name: COLUMN "sys_role_dept"."role_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role_dept"."role_id" IS '角色ID';


--
-- Name: COLUMN "sys_role_dept"."dept_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role_dept"."dept_id" IS '部门ID';


--
-- Name: sys_role_menu; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_role_menu" (
    "role_id" BIGINT NOT NULL,
    "menu_id" BIGINT NOT NULL
);


ALTER TABLE "PUBLIC"."sys_role_menu" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_role_menu"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_role_menu" IS '角色和菜单关联表';


--
-- Name: COLUMN "sys_role_menu"."role_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role_menu"."role_id" IS '角色ID';


--
-- Name: COLUMN "sys_role_menu"."menu_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_role_menu"."menu_id" IS '菜单ID';


--
-- Name: sys_role_role_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_role_role_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_role_role_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_role_role_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_role_role_id_SEQ" OWNED BY "PUBLIC"."sys_role"."role_id";


--
-- Name: sys_user; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_user" (
    "user_id" BIGINT NOT NULL,
    "dept_id" BIGINT,
    "login_name" CHARACTER VARYING(30 char) NOT NULL,
    "user_name" CHARACTER VARYING(30 char) DEFAULT NULL::CHARACTER VARYING,
    "user_type" CHARACTER VARYING(2 char) DEFAULT '00'::CHARACTER VARYING,
    "email" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "phonenumber" CHARACTER VARYING(11 char) DEFAULT NULL::CHARACTER VARYING,
    "sex" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "avatar" CHARACTER VARYING(100 char) DEFAULT NULL::CHARACTER VARYING,
    "password" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "salt" CHARACTER VARYING(20 char) DEFAULT NULL::CHARACTER VARYING,
    "status" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "del_flag" CHARACTER(1 char) DEFAULT '0'::BPCHAR,
    "login_ip" CHARACTER VARYING(128 char) DEFAULT NULL::CHARACTER VARYING,
    "login_date" TIMESTAMP WITHOUT TIME ZONE,
    "pwd_update_date" TIMESTAMP WITHOUT TIME ZONE,
    "create_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "create_time" TIMESTAMP WITHOUT TIME ZONE,
    "update_by" CHARACTER VARYING(64 char) DEFAULT NULL::CHARACTER VARYING,
    "update_time" TIMESTAMP WITHOUT TIME ZONE,
    "remark" CHARACTER VARYING(500 char)
);


ALTER TABLE "PUBLIC"."sys_user" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_user"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_user" IS '用户信息表';


--
-- Name: COLUMN "sys_user"."user_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."user_id" IS '用户ID';


--
-- Name: COLUMN "sys_user"."dept_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."dept_id" IS '部门ID';


--
-- Name: COLUMN "sys_user"."login_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."login_name" IS '登录账号';


--
-- Name: COLUMN "sys_user"."user_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."user_name" IS '用户昵称';


--
-- Name: COLUMN "sys_user"."user_type"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."user_type" IS '用户类型（00系统用户 01注册用户）';


--
-- Name: COLUMN "sys_user"."email"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."email" IS '用户邮箱';


--
-- Name: COLUMN "sys_user"."phonenumber"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."phonenumber" IS '手机号码';


--
-- Name: COLUMN "sys_user"."sex"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."sex" IS '用户性别（0男 1女 2未知）';


--
-- Name: COLUMN "sys_user"."avatar"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."avatar" IS '头像路径';


--
-- Name: COLUMN "sys_user"."password"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."password" IS '密码';


--
-- Name: COLUMN "sys_user"."salt"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."salt" IS '盐加密';


--
-- Name: COLUMN "sys_user"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."status" IS '帐号状态（0正常 1停用）';


--
-- Name: COLUMN "sys_user"."del_flag"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."del_flag" IS '删除标志（0代表存在 2代表删除）';


--
-- Name: COLUMN "sys_user"."login_ip"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."login_ip" IS '最后登录IP';


--
-- Name: COLUMN "sys_user"."login_date"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."login_date" IS '最后登录时间';


--
-- Name: COLUMN "sys_user"."pwd_update_date"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."pwd_update_date" IS '密码最后更新时间';


--
-- Name: COLUMN "sys_user"."create_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."create_by" IS '创建者';


--
-- Name: COLUMN "sys_user"."create_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."create_time" IS '创建时间';


--
-- Name: COLUMN "sys_user"."update_by"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."update_by" IS '更新者';


--
-- Name: COLUMN "sys_user"."update_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."update_time" IS '更新时间';


--
-- Name: COLUMN "sys_user"."remark"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user"."remark" IS '备注';


--
-- Name: sys_user_online; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_user_online" (
    "sessionId" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING NOT NULL,
    "login_name" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "dept_name" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "ipaddr" CHARACTER VARYING(128 char) DEFAULT NULL::CHARACTER VARYING,
    "login_location" CHARACTER VARYING(255 char) DEFAULT NULL::CHARACTER VARYING,
    "browser" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "os" CHARACTER VARYING(50 char) DEFAULT NULL::CHARACTER VARYING,
    "status" CHARACTER VARYING(10 char) DEFAULT NULL::CHARACTER VARYING,
    "start_timestamp" TIMESTAMP WITHOUT TIME ZONE,
    "last_access_time" TIMESTAMP WITHOUT TIME ZONE,
    "expire_time" INTEGER DEFAULT 0
);


ALTER TABLE "PUBLIC"."sys_user_online" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_user_online"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_user_online" IS '在线用户记录';


--
-- Name: COLUMN "sys_user_online"."sessionId"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."sessionId" IS '用户会话id';


--
-- Name: COLUMN "sys_user_online"."login_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."login_name" IS '登录账号';


--
-- Name: COLUMN "sys_user_online"."dept_name"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."dept_name" IS '部门名称';


--
-- Name: COLUMN "sys_user_online"."ipaddr"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."ipaddr" IS '登录IP地址';


--
-- Name: COLUMN "sys_user_online"."login_location"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."login_location" IS '登录地点';


--
-- Name: COLUMN "sys_user_online"."browser"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."browser" IS '浏览器类型';


--
-- Name: COLUMN "sys_user_online"."os"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."os" IS '操作系统';


--
-- Name: COLUMN "sys_user_online"."status"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."status" IS '在线状态on_line在线off_line离线';


--
-- Name: COLUMN "sys_user_online"."start_timestamp"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."start_timestamp" IS 'session创建时间';


--
-- Name: COLUMN "sys_user_online"."last_access_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."last_access_time" IS 'session最后访问时间';


--
-- Name: COLUMN "sys_user_online"."expire_time"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_online"."expire_time" IS '超时时间，单位为分钟';


--
-- Name: sys_user_post; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_user_post" (
    "user_id" BIGINT NOT NULL,
    "post_id" BIGINT NOT NULL
);


ALTER TABLE "PUBLIC"."sys_user_post" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_user_post"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_user_post" IS '用户与岗位关联表';


--
-- Name: COLUMN "sys_user_post"."user_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_post"."user_id" IS '用户ID';


--
-- Name: COLUMN "sys_user_post"."post_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_post"."post_id" IS '岗位ID';


--
-- Name: sys_user_role; Type: TABLE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE TABLE "PUBLIC"."sys_user_role" (
    "user_id" BIGINT NOT NULL,
    "role_id" BIGINT NOT NULL
);


ALTER TABLE "PUBLIC"."sys_user_role" OWNER TO "SYSTEM";

--
-- Name: TABLE "sys_user_role"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON TABLE "PUBLIC"."sys_user_role" IS '用户和角色关联表';


--
-- Name: COLUMN "sys_user_role"."user_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_role"."user_id" IS '用户ID';


--
-- Name: COLUMN "sys_user_role"."role_id"; Type: COMMENT; Schema: PUBLIC; Owner: SYSTEM
--

COMMENT ON COLUMN "PUBLIC"."sys_user_role"."role_id" IS '角色ID';


--
-- Name: sys_user_user_id_SEQ; Type: SEQUENCE; Schema: PUBLIC; Owner: SYSTEM
--

CREATE SEQUENCE "PUBLIC"."sys_user_user_id_SEQ"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "PUBLIC"."sys_user_user_id_SEQ" OWNER TO "SYSTEM";

--
-- Name: sys_user_user_id_SEQ; Type: SEQUENCE OWNED BY; Schema: PUBLIC; Owner: SYSTEM
--

ALTER SEQUENCE "PUBLIC"."sys_user_user_id_SEQ" OWNED BY "PUBLIC"."sys_user"."user_id";


--
-- Name: gen_table table_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."gen_table" ALTER COLUMN "table_id" SET DEFAULT NEXTVAL('PUBLIC.gen_table_table_id_SEQ'::REGCLASS);


--
-- Name: gen_table_column column_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."gen_table_column" ALTER COLUMN "column_id" SET DEFAULT NEXTVAL('PUBLIC.gen_table_column_column_id_SEQ'::REGCLASS);


--
-- Name: sys_config config_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_config" ALTER COLUMN "config_id" SET DEFAULT NEXTVAL('PUBLIC.sys_config_config_id_SEQ'::REGCLASS);


--
-- Name: sys_dept dept_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dept" ALTER COLUMN "dept_id" SET DEFAULT NEXTVAL('PUBLIC.sys_dept_dept_id_SEQ'::REGCLASS);


--
-- Name: sys_dict_data dict_code; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dict_data" ALTER COLUMN "dict_code" SET DEFAULT NEXTVAL('PUBLIC.sys_dict_data_dict_code_SEQ'::REGCLASS);


--
-- Name: sys_dict_type dict_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dict_type" ALTER COLUMN "dict_id" SET DEFAULT NEXTVAL('PUBLIC.sys_dict_type_dict_id_SEQ'::REGCLASS);


--
-- Name: sys_job job_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_job" ALTER COLUMN "job_id" SET DEFAULT NEXTVAL('PUBLIC.sys_job_job_id_SEQ'::REGCLASS);


--
-- Name: sys_job_log job_log_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_job_log" ALTER COLUMN "job_log_id" SET DEFAULT NEXTVAL('PUBLIC.sys_job_log_job_log_id_SEQ'::REGCLASS);


--
-- Name: sys_logininfor info_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_logininfor" ALTER COLUMN "info_id" SET DEFAULT NEXTVAL('PUBLIC.sys_logininfor_info_id_SEQ'::REGCLASS);


--
-- Name: sys_menu menu_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_menu" ALTER COLUMN "menu_id" SET DEFAULT NEXTVAL('PUBLIC.sys_menu_menu_id_SEQ'::REGCLASS);


--
-- Name: sys_notice notice_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_notice" ALTER COLUMN "notice_id" SET DEFAULT NEXTVAL('PUBLIC.sys_notice_notice_id_SEQ'::REGCLASS);


--
-- Name: sys_oper_log oper_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_oper_log" ALTER COLUMN "oper_id" SET DEFAULT NEXTVAL('PUBLIC.sys_oper_log_oper_id_SEQ'::REGCLASS);


--
-- Name: sys_post post_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_post" ALTER COLUMN "post_id" SET DEFAULT NEXTVAL('PUBLIC.sys_post_post_id_SEQ'::REGCLASS);


--
-- Name: sys_role role_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_role" ALTER COLUMN "role_id" SET DEFAULT NEXTVAL('PUBLIC.sys_role_role_id_SEQ'::REGCLASS);


--
-- Name: sys_user user_id; Type: DEFAULT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_user" ALTER COLUMN "user_id" SET DEFAULT NEXTVAL('PUBLIC.sys_user_user_id_SEQ'::REGCLASS);


--
-- Data for Name: gen_table; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."gen_table" ("table_id", "table_name", "table_comment", "sub_table_name", "sub_table_fk_name", "class_name", "tpl_category", "package_name", "module_name", "business_name", "function_name", "function_author", "gen_type", "gen_path", "options", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
\.


--
-- Data for Name: gen_table_column; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."gen_table_column" ("column_id", "table_id", "column_name", "column_comment", "column_type", "java_type", "java_field", "is_pk", "is_increment", "is_required", "is_insert", "is_edit", "is_list", "is_query", "query_type", "html_type", "dict_type", "sort", "create_by", "create_time", "update_by", "update_time") FROM stdin;
\.


--
-- Name: gen_table_column_column_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."gen_table_column_column_id_SEQ"', 0, true);


--
-- Name: gen_table_table_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."gen_table_table_id_SEQ"', 0, true);


--
-- Data for Name: sys_config; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_config" ("config_id", "config_name", "config_key", "config_value", "config_type", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
2	用户管理-账号初始密码	sys.user.initPassword	123456	Y	admin	2021-02-24 10:17:13		\N	初始化密码 123456
4	账号自助-是否开启用户注册功能	sys.account.registerUser	false	Y	admin	2021-02-24 10:17:13		\N	是否开启注册用户功能（true开启，false关闭）
6	用户管理-初始密码修改策略	sys.account.initPasswordModify	0	Y	admin	2021-02-24 10:17:13		\N	0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框
8	主框架页-菜单导航显示风格	sys.index.menuStyle	default	Y	admin	2021-02-24 10:17:13		\N	菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）
1	主框架页-默认皮肤样式名称	sys.index.skinName	skin-blue	Y	admin	2021-02-24 10:17:13		\N	蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow
3	主框架页-侧边栏主题	sys.index.sideTheme	theme-dark	Y	admin	2021-02-24 10:17:13		\N	深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue
5	用户管理-密码字符范围	sys.account.chrtype	0	Y	admin	2021-02-24 10:17:13		\N	默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）
7	用户管理-账号密码更新周期	sys.account.passwordValidateDays	0	Y	admin	2021-02-24 10:17:13		\N	密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框
9	主框架页-是否开启页脚	sys.index.ignoreFooter	true	Y	admin	2021-02-24 10:17:13		\N	是否开启底部页脚显示（true显示，false隐藏）
\.


--
-- Name: sys_config_config_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_config_config_id_SEQ"', 9, true);


--
-- Data for Name: sys_dept; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_dept" ("dept_id", "parent_id", "ancestors", "dept_name", "order_num", "leader", "phone", "email", "status", "del_flag", "create_by", "create_time", "update_by", "update_time") FROM stdin;
100	0	0	若依科技	0	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
102	100	0,100	长沙分公司	2	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
104	101	0,100,101	市场部门	2	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
106	101	0,100,101	财务部门	4	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
108	102	0,100,102	市场部门	1	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
101	100	0,100	深圳总公司	1	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
103	101	0,100,101	研发部门	1	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
105	101	0,100,101	测试部门	3	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
107	101	0,100,101	运维部门	5	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
109	102	0,100,102	财务部门	2	若依	15888888888	ry@qq.com	0	0	admin	2021-02-24 10:17:11		\N
\.


--
-- Name: sys_dept_dept_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_dept_dept_id_SEQ"', 109, true);


--
-- Data for Name: sys_dict_data; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_dict_data" ("dict_code", "dict_sort", "dict_label", "dict_value", "dict_type", "css_class", "list_class", "is_default", "status", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
2	2	女	1	sys_user_sex			N	0	admin	2021-02-24 10:17:13		\N	性别女
4	1	显示	0	sys_show_hide		primary	Y	0	admin	2021-02-24 10:17:13		\N	显示菜单
6	1	正常	0	sys_normal_disable		primary	Y	0	admin	2021-02-24 10:17:13		\N	正常状态
8	1	正常	0	sys_job_status		primary	Y	0	admin	2021-02-24 10:17:13		\N	正常状态
10	1	默认	DEFAULT	sys_job_group			Y	0	admin	2021-02-24 10:17:13		\N	默认分组
12	1	是	Y	sys_yes_no		primary	Y	0	admin	2021-02-24 10:17:13		\N	系统默认是
14	1	通知	1	sys_notice_type		warning	Y	0	admin	2021-02-24 10:17:13		\N	通知
16	1	正常	0	sys_notice_status		primary	Y	0	admin	2021-02-24 10:17:13		\N	正常状态
18	99	其他	0	sys_oper_type		info	N	0	admin	2021-02-24 10:17:13		\N	其他操作
20	2	修改	2	sys_oper_type		info	N	0	admin	2021-02-24 10:17:13		\N	修改操作
22	4	授权	4	sys_oper_type		primary	N	0	admin	2021-02-24 10:17:13		\N	授权操作
24	6	导入	6	sys_oper_type		warning	N	0	admin	2021-02-24 10:17:13		\N	导入操作
26	8	生成代码	8	sys_oper_type		warning	N	0	admin	2021-02-24 10:17:13		\N	生成操作
28	1	成功	0	sys_common_status		primary	N	0	admin	2021-02-24 10:17:13		\N	正常状态
1	1	男	0	sys_user_sex			Y	0	admin	2021-02-24 10:17:13		\N	性别男
3	3	未知	2	sys_user_sex			N	0	admin	2021-02-24 10:17:13		\N	性别未知
5	2	隐藏	1	sys_show_hide		danger	N	0	admin	2021-02-24 10:17:13		\N	隐藏菜单
7	2	停用	1	sys_normal_disable		danger	N	0	admin	2021-02-24 10:17:13		\N	停用状态
9	2	暂停	1	sys_job_status		danger	N	0	admin	2021-02-24 10:17:13		\N	停用状态
11	2	系统	SYSTEM	sys_job_group			N	0	admin	2021-02-24 10:17:13		\N	系统分组
13	2	否	N	sys_yes_no		danger	N	0	admin	2021-02-24 10:17:13		\N	系统默认否
15	2	公告	2	sys_notice_type		success	N	0	admin	2021-02-24 10:17:13		\N	公告
17	2	关闭	1	sys_notice_status		danger	N	0	admin	2021-02-24 10:17:13		\N	关闭状态
19	1	新增	1	sys_oper_type		info	N	0	admin	2021-02-24 10:17:13		\N	新增操作
21	3	删除	3	sys_oper_type		danger	N	0	admin	2021-02-24 10:17:13		\N	删除操作
23	5	导出	5	sys_oper_type		warning	N	0	admin	2021-02-24 10:17:13		\N	导出操作
25	7	强退	7	sys_oper_type		danger	N	0	admin	2021-02-24 10:17:13		\N	强退操作
27	9	清空数据	9	sys_oper_type		danger	N	0	admin	2021-02-24 10:17:13		\N	清空操作
29	2	失败	1	sys_common_status		danger	N	0	admin	2021-02-24 10:17:13		\N	停用状态
\.


--
-- Name: sys_dict_data_dict_code_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_dict_data_dict_code_SEQ"', 29, true);


--
-- Data for Name: sys_dict_type; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_dict_type" ("dict_id", "dict_name", "dict_type", "status", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
1	用户性别	sys_user_sex	0	admin	2021-02-24 10:17:12		\N	用户性别列表
3	系统开关	sys_normal_disable	0	admin	2021-02-24 10:17:12		\N	系统开关列表
5	任务分组	sys_job_group	0	admin	2021-02-24 10:17:13		\N	任务分组列表
7	通知类型	sys_notice_type	0	admin	2021-02-24 10:17:13		\N	通知类型列表
9	操作类型	sys_oper_type	0	admin	2021-02-24 10:17:13		\N	操作类型列表
2	菜单状态	sys_show_hide	0	admin	2021-02-24 10:17:12		\N	菜单状态列表
4	任务状态	sys_job_status	0	admin	2021-02-24 10:17:13		\N	任务状态列表
6	系统是否	sys_yes_no	0	admin	2021-02-24 10:17:13		\N	系统是否列表
8	通知状态	sys_notice_status	0	admin	2021-02-24 10:17:13		\N	通知状态列表
10	系统状态	sys_common_status	0	admin	2021-02-24 10:17:13		\N	登录状态列表
\.


--
-- Name: sys_dict_type_dict_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_dict_type_dict_id_SEQ"', 10, true);


--
-- Data for Name: sys_job; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_job" ("job_id", "job_name", "job_group", "invoke_target", "cron_expression", "misfire_policy", "concurrent", "status", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
2	系统默认（有参）	DEFAULT	ryTask.ryParams('ry')	0/15 * * * * ?	3	1	1	admin	2021-02-24 10:17:13		\N
1	系统默认（无参）	DEFAULT	ryTask.ryNoParams	0/10 * * * * ?	3	1	1	admin	2021-02-24 10:17:13		\N
3	系统默认（多参）	DEFAULT	ryTask.ryMultipleParams('ry', true, 2000L, 316.50D, 100)	0/20 * * * * ?	3	1	1	admin	2021-02-24 10:17:13		\N
\.


--
-- Name: sys_job_job_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_job_job_id_SEQ"', 3, true);


--
-- Data for Name: sys_job_log; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_job_log" ("job_log_id", "job_name", "job_group", "invoke_target", "job_message", "status", "exception_info", "create_time") FROM stdin;
\.


--
-- Name: sys_job_log_job_log_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_job_log_job_log_id_SEQ"', 0, true);


--
-- Data for Name: sys_logininfor; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_logininfor" ("info_id", "login_name", "ipaddr", "login_location", "browser", "os", "status", "msg", "login_time") FROM stdin;
1	admin	127.0.0.1	内网IP	Chrome 8	Mac OS X	1	密码输入错误1次	2021-02-24 16:10:26
2	admin	127.0.0.1	内网IP	Chrome 8	Mac OS X	0	登录成功	2021-02-24 16:11:25
3	admin	127.0.0.1	内网IP	Chrome 8	Mac OS X	0	登录成功	2021-02-25 00:00:00
4	admin	127.0.0.1	内网IP	Chrome 8	Mac OS X	0	登录成功	2021-02-25 00:00:00
5	admin	127.0.0.1	内网IP	Chrome 8	Mac OS X	0	登录成功	2021-02-25 00:00:00
\.


--
-- Name: sys_logininfor_info_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_logininfor_info_id_SEQ"', 5, true);


--
-- Data for Name: sys_menu; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_menu" ("menu_id", "menu_name", "parent_id", "order_num", "url", "target", "menu_type", "visible", "is_refresh", "perms", "icon", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
2	系统监控	0	2	#		M	0	1		fa fa-video-camera	admin	2021-02-24 10:17:11		\N	系统监控目录
4	若依官网	0	4	http://ruoyi.vip	menuBlank	C	0	1		fa fa-location-arrow	admin	2021-02-24 10:17:11		\N	若依官网地址
101	角色管理	1	2	/system/role		C	0	1	system:role:view	fa fa-user-secret	admin	2021-02-24 10:17:11		\N	角色管理菜单
103	部门管理	1	4	/system/dept		C	0	1	system:dept:view	fa fa-outdent	admin	2021-02-24 10:17:11		\N	部门管理菜单
105	字典管理	1	6	/system/dict		C	0	1	system:dict:view	fa fa-bookmark-o	admin	2021-02-24 10:17:11		\N	字典管理菜单
107	通知公告	1	8	/system/notice		C	0	1	system:notice:view	fa fa-bullhorn	admin	2021-02-24 10:17:11		\N	通知公告菜单
109	在线用户	2	1	/monitor/online		C	0	1	monitor:online:view	fa fa-user-circle	admin	2021-02-24 10:17:11		\N	在线用户菜单
111	数据监控	2	3	/monitor/data		C	0	1	monitor:data:view	fa fa-bug	admin	2021-02-24 10:17:11		\N	数据监控菜单
113	缓存监控	2	5	/monitor/cache		C	0	1	monitor:cache:view	fa fa-cube	admin	2021-02-24 10:17:11		\N	缓存监控菜单
115	代码生成	3	2	/tool/gen		C	0	1	tool:gen:view	fa fa-code	admin	2021-02-24 10:17:11		\N	代码生成菜单
500	操作日志	108	1	/monitor/operlog		C	0	1	monitor:operlog:view	fa fa-address-book	admin	2021-02-24 10:17:11		\N	操作日志菜单
1000	用户查询	100	1	#		F	0	1	system:user:list	#	admin	2021-02-24 10:17:11		\N
1002	用户修改	100	3	#		F	0	1	system:user:edit	#	admin	2021-02-24 10:17:11		\N
1004	用户导出	100	5	#		F	0	1	system:user:export	#	admin	2021-02-24 10:17:11		\N
1006	重置密码	100	7	#		F	0	1	system:user:resetPwd	#	admin	2021-02-24 10:17:11		\N
1008	角色新增	101	2	#		F	0	1	system:role:add	#	admin	2021-02-24 10:17:11		\N
1010	角色删除	101	4	#		F	0	1	system:role:remove	#	admin	2021-02-24 10:17:11		\N
1012	菜单查询	102	1	#		F	0	1	system:menu:list	#	admin	2021-02-24 10:17:11		\N
1014	菜单修改	102	3	#		F	0	1	system:menu:edit	#	admin	2021-02-24 10:17:11		\N
1016	部门查询	103	1	#		F	0	1	system:dept:list	#	admin	2021-02-24 10:17:11		\N
1018	部门修改	103	3	#		F	0	1	system:dept:edit	#	admin	2021-02-24 10:17:11		\N
1020	岗位查询	104	1	#		F	0	1	system:post:list	#	admin	2021-02-24 10:17:12		\N
1022	岗位修改	104	3	#		F	0	1	system:post:edit	#	admin	2021-02-24 10:17:12		\N
1024	岗位导出	104	5	#		F	0	1	system:post:export	#	admin	2021-02-24 10:17:12		\N
1026	字典新增	105	2	#		F	0	1	system:dict:add	#	admin	2021-02-24 10:17:12		\N
1028	字典删除	105	4	#		F	0	1	system:dict:remove	#	admin	2021-02-24 10:17:12		\N
1030	参数查询	106	1	#		F	0	1	system:config:list	#	admin	2021-02-24 10:17:12		\N
1032	参数修改	106	3	#		F	0	1	system:config:edit	#	admin	2021-02-24 10:17:12		\N
1034	参数导出	106	5	#		F	0	1	system:config:export	#	admin	2021-02-24 10:17:12		\N
1036	公告新增	107	2	#		F	0	1	system:notice:add	#	admin	2021-02-24 10:17:12		\N
1038	公告删除	107	4	#		F	0	1	system:notice:remove	#	admin	2021-02-24 10:17:12		\N
1040	操作删除	500	2	#		F	0	1	monitor:operlog:remove	#	admin	2021-02-24 10:17:12		\N
1042	日志导出	500	4	#		F	0	1	monitor:operlog:export	#	admin	2021-02-24 10:17:12		\N
1044	登录删除	501	2	#		F	0	1	monitor:logininfor:remove	#	admin	2021-02-24 10:17:12		\N
1046	账户解锁	501	4	#		F	0	1	monitor:logininfor:unlock	#	admin	2021-02-24 10:17:12		\N
1048	批量强退	109	2	#		F	0	1	monitor:online:batchForceLogout	#	admin	2021-02-24 10:17:12		\N
1050	任务查询	110	1	#		F	0	1	monitor:job:list	#	admin	2021-02-24 10:17:12		\N
1052	任务修改	110	3	#		F	0	1	monitor:job:edit	#	admin	2021-02-24 10:17:12		\N
1054	状态修改	110	5	#		F	0	1	monitor:job:changeStatus	#	admin	2021-02-24 10:17:12		\N
1056	任务导出	110	7	#		F	0	1	monitor:job:export	#	admin	2021-02-24 10:17:12		\N
1058	生成修改	115	2	#		F	0	1	tool:gen:edit	#	admin	2021-02-24 10:17:12		\N
1060	预览代码	115	4	#		F	0	1	tool:gen:preview	#	admin	2021-02-24 10:17:12		\N
1	系统管理	0	1	#		M	0	1		fa fa-gear	admin	2021-02-24 10:17:11		\N	系统管理目录
3	系统工具	0	3	#		M	0	1		fa fa-bars	admin	2021-02-24 10:17:11		\N	系统工具目录
100	用户管理	1	1	/system/user		C	0	1	system:user:view	fa fa-user-o	admin	2021-02-24 10:17:11		\N	用户管理菜单
102	菜单管理	1	3	/system/menu		C	0	1	system:menu:view	fa fa-th-list	admin	2021-02-24 10:17:11		\N	菜单管理菜单
104	岗位管理	1	5	/system/post		C	0	1	system:post:view	fa fa-address-card-o	admin	2021-02-24 10:17:11		\N	岗位管理菜单
106	参数设置	1	7	/system/config		C	0	1	system:config:view	fa fa-sun-o	admin	2021-02-24 10:17:11		\N	参数设置菜单
108	日志管理	1	9	#		M	0	1		fa fa-pencil-square-o	admin	2021-02-24 10:17:11		\N	日志管理菜单
110	定时任务	2	2	/monitor/job		C	0	1	monitor:job:view	fa fa-tasks	admin	2021-02-24 10:17:11		\N	定时任务菜单
112	服务监控	2	4	/monitor/server		C	0	1	monitor:server:view	fa fa-server	admin	2021-02-24 10:17:11		\N	服务监控菜单
114	表单构建	3	1	/tool/build		C	0	1	tool:build:view	fa fa-wpforms	admin	2021-02-24 10:17:11		\N	表单构建菜单
116	系统接口	3	3	/tool/swagger		C	0	1	tool:swagger:view	fa fa-gg	admin	2021-02-24 10:17:11		\N	系统接口菜单
501	登录日志	108	2	/monitor/logininfor		C	0	1	monitor:logininfor:view	fa fa-file-image-o	admin	2021-02-24 10:17:11		\N	登录日志菜单
1001	用户新增	100	2	#		F	0	1	system:user:add	#	admin	2021-02-24 10:17:11		\N
1003	用户删除	100	4	#		F	0	1	system:user:remove	#	admin	2021-02-24 10:17:11		\N
1005	用户导入	100	6	#		F	0	1	system:user:import	#	admin	2021-02-24 10:17:11		\N
1007	角色查询	101	1	#		F	0	1	system:role:list	#	admin	2021-02-24 10:17:11		\N
1009	角色修改	101	3	#		F	0	1	system:role:edit	#	admin	2021-02-24 10:17:11		\N
1011	角色导出	101	5	#		F	0	1	system:role:export	#	admin	2021-02-24 10:17:11		\N
1013	菜单新增	102	2	#		F	0	1	system:menu:add	#	admin	2021-02-24 10:17:11		\N
1015	菜单删除	102	4	#		F	0	1	system:menu:remove	#	admin	2021-02-24 10:17:11		\N
1017	部门新增	103	2	#		F	0	1	system:dept:add	#	admin	2021-02-24 10:17:11		\N
1019	部门删除	103	4	#		F	0	1	system:dept:remove	#	admin	2021-02-24 10:17:12		\N
1021	岗位新增	104	2	#		F	0	1	system:post:add	#	admin	2021-02-24 10:17:12		\N
1023	岗位删除	104	4	#		F	0	1	system:post:remove	#	admin	2021-02-24 10:17:12		\N
1025	字典查询	105	1	#		F	0	1	system:dict:list	#	admin	2021-02-24 10:17:12		\N
1027	字典修改	105	3	#		F	0	1	system:dict:edit	#	admin	2021-02-24 10:17:12		\N
1029	字典导出	105	5	#		F	0	1	system:dict:export	#	admin	2021-02-24 10:17:12		\N
1031	参数新增	106	2	#		F	0	1	system:config:add	#	admin	2021-02-24 10:17:12		\N
1033	参数删除	106	4	#		F	0	1	system:config:remove	#	admin	2021-02-24 10:17:12		\N
1035	公告查询	107	1	#		F	0	1	system:notice:list	#	admin	2021-02-24 10:17:12		\N
1037	公告修改	107	3	#		F	0	1	system:notice:edit	#	admin	2021-02-24 10:17:12		\N
1039	操作查询	500	1	#		F	0	1	monitor:operlog:list	#	admin	2021-02-24 10:17:12		\N
1041	详细信息	500	3	#		F	0	1	monitor:operlog:detail	#	admin	2021-02-24 10:17:12		\N
1043	登录查询	501	1	#		F	0	1	monitor:logininfor:list	#	admin	2021-02-24 10:17:12		\N
1045	日志导出	501	3	#		F	0	1	monitor:logininfor:export	#	admin	2021-02-24 10:17:12		\N
1047	在线查询	109	1	#		F	0	1	monitor:online:list	#	admin	2021-02-24 10:17:12		\N
1049	单条强退	109	3	#		F	0	1	monitor:online:forceLogout	#	admin	2021-02-24 10:17:12		\N
1051	任务新增	110	2	#		F	0	1	monitor:job:add	#	admin	2021-02-24 10:17:12		\N
1053	任务删除	110	4	#		F	0	1	monitor:job:remove	#	admin	2021-02-24 10:17:12		\N
1055	任务详细	110	6	#		F	0	1	monitor:job:detail	#	admin	2021-02-24 10:17:12		\N
1057	生成查询	115	1	#		F	0	1	tool:gen:list	#	admin	2021-02-24 10:17:12		\N
1059	生成删除	115	3	#		F	0	1	tool:gen:remove	#	admin	2021-02-24 10:17:12		\N
1061	生成代码	115	5	#		F	0	1	tool:gen:code	#	admin	2021-02-24 10:17:12		\N
\.


--
-- Name: sys_menu_menu_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_menu_menu_id_SEQ"', 1061, true);


--
-- Data for Name: sys_notice; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_notice" ("notice_id", "notice_title", "notice_type", "notice_content", "status", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
1	温馨提醒：2018-07-01 若依新版本发布啦	2	新版本内容	0	admin	2021-02-24 10:17:13		\N	管理员
2	维护通知：2018-07-01 若依系统凌晨维护	1	维护内容	0	admin	2021-02-24 10:17:13		\N	管理员
\.


--
-- Name: sys_notice_notice_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_notice_notice_id_SEQ"', 2, true);


--
-- Data for Name: sys_oper_log; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_oper_log" ("oper_id", "title", "business_type", "method", "request_method", "operator_type", "oper_name", "dept_name", "oper_url", "oper_ip", "oper_location", "oper_param", "json_result", "status", "error_msg", "oper_time") FROM stdin;
\.


--
-- Name: sys_oper_log_oper_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_oper_log_oper_id_SEQ"', 0, true);


--
-- Data for Name: sys_post; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_post" ("post_id", "post_code", "post_name", "post_sort", "status", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
1	ceo	董事长	1	0	admin	2021-02-24 10:17:11		\N
3	hr	人力资源	3	0	admin	2021-02-24 10:17:11		\N
2	se	项目经理	2	0	admin	2021-02-24 10:17:11		\N
4	user	普通员工	4	0	admin	2021-02-24 10:17:11		\N
\.


--
-- Name: sys_post_post_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_post_post_id_SEQ"', 4, true);


--
-- Data for Name: sys_role; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_role" ("role_id", "role_name", "role_key", "role_sort", "data_scope", "status", "del_flag", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
1	超级管理员	admin	1	1	0	0	admin	2021-02-24 10:17:11		\N	超级管理员
2	普通角色	common	2	2	0	0	admin	2021-02-24 10:17:11		\N	普通角色
\.


--
-- Data for Name: sys_role_dept; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_role_dept" ("role_id", "dept_id") FROM stdin;
2	100
2	105
2	101
\.


--
-- Data for Name: sys_role_menu; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_role_menu" ("role_id", "menu_id") FROM stdin;
2	2
2	4
2	101
2	103
2	105
2	107
2	109
2	111
2	113
2	115
2	500
2	1000
2	1002
2	1004
2	1006
2	1008
2	1010
2	1012
2	1014
2	1016
2	1018
2	1020
2	1022
2	1024
2	1026
2	1028
2	1030
2	1032
2	1034
2	1036
2	1038
2	1040
2	1042
2	1044
2	1046
2	1048
2	1050
2	1052
2	1054
2	1056
2	1058
2	1060
2	1
2	3
2	100
2	102
2	104
2	106
2	108
2	110
2	112
2	114
2	116
2	501
2	1001
2	1003
2	1005
2	1007
2	1009
2	1011
2	1013
2	1015
2	1017
2	1019
2	1021
2	1023
2	1025
2	1027
2	1029
2	1031
2	1033
2	1035
2	1037
2	1039
2	1041
2	1043
2	1045
2	1047
2	1049
2	1051
2	1053
2	1055
2	1057
2	1059
2	1061
\.


--
-- Name: sys_role_role_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_role_role_id_SEQ"', 2, true);


--
-- Data for Name: sys_user; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_user" ("user_id", "dept_id", "login_name", "user_name", "user_type", "email", "phonenumber", "sex", "avatar", "password", "salt", "status", "del_flag", "login_ip", "login_date", "pwd_update_date", "create_by", "create_time", "update_by", "update_time", "remark") FROM stdin;
2	105	ry	若依	00	ry@qq.com	15666666666	1		8e6d98b90472783cc73c17047ddccf36	222222	0	0	127.0.0.1	2021-02-24 10:17:11	2021-02-24 10:17:11	admin	2021-02-24 10:17:11		\N	测试员
1	103	admin	若依	00	ry@163.com	15888888888	1		29c67a30398638269fe600f73a054934	111111	0	0	127.0.0.1	2021-02-25 15:57:27.046000	2021-02-24 10:17:11	admin	2021-02-24 10:17:11		2021-02-25 00:00:00	管理员
\.


--
-- Data for Name: sys_user_online; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_user_online" ("sessionId", "login_name", "dept_name", "ipaddr", "login_location", "browser", "os", "status", "start_timestamp", "last_access_time", "expire_time") FROM stdin;
\.


--
-- Data for Name: sys_user_post; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_user_post" ("user_id", "post_id") FROM stdin;
1	1
2	2
\.


--
-- Data for Name: sys_user_role; Type: TABLE DATA; Schema: PUBLIC; Owner: SYSTEM
--

COPY "PUBLIC"."sys_user_role" ("user_id", "role_id") FROM stdin;
1	1
2	2
\.


--
-- Name: sys_user_user_id_SEQ; Type: SEQUENCE SET; Schema: PUBLIC; Owner: SYSTEM
--

SELECT sys_catalog.setval('"PUBLIC"."sys_user_user_id_SEQ"', 2, true);


--
-- Name: sys_dict_type dict_type; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dict_type"
    ADD CONSTRAINT "dict_type" UNIQUE ("dict_type");


--
-- Name: gen_table gen_table_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."gen_table"
    ADD CONSTRAINT "gen_table_PKEY" PRIMARY KEY ("table_id");


--
-- Name: gen_table_column gen_table_column_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."gen_table_column"
    ADD CONSTRAINT "gen_table_column_PKEY" PRIMARY KEY ("column_id");


--
-- Name: sys_config sys_config_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_config"
    ADD CONSTRAINT "sys_config_PKEY" PRIMARY KEY ("config_id");


--
-- Name: sys_dept sys_dept_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dept"
    ADD CONSTRAINT "sys_dept_PKEY" PRIMARY KEY ("dept_id");


--
-- Name: sys_dict_data sys_dict_data_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dict_data"
    ADD CONSTRAINT "sys_dict_data_PKEY" PRIMARY KEY ("dict_code");


--
-- Name: sys_dict_type sys_dict_type_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_dict_type"
    ADD CONSTRAINT "sys_dict_type_PKEY" PRIMARY KEY ("dict_id");


--
-- Name: sys_job sys_job_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_job"
    ADD CONSTRAINT "sys_job_PKEY" PRIMARY KEY ("job_group", "job_id", "job_name");


--
-- Name: sys_job_log sys_job_log_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_job_log"
    ADD CONSTRAINT "sys_job_log_PKEY" PRIMARY KEY ("job_log_id");


--
-- Name: sys_logininfor sys_logininfor_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_logininfor"
    ADD CONSTRAINT "sys_logininfor_PKEY" PRIMARY KEY ("info_id");


--
-- Name: sys_menu sys_menu_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_menu"
    ADD CONSTRAINT "sys_menu_PKEY" PRIMARY KEY ("menu_id");


--
-- Name: sys_notice sys_notice_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_notice"
    ADD CONSTRAINT "sys_notice_PKEY" PRIMARY KEY ("notice_id");


--
-- Name: sys_oper_log sys_oper_log_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_oper_log"
    ADD CONSTRAINT "sys_oper_log_PKEY" PRIMARY KEY ("oper_id");


--
-- Name: sys_post sys_post_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_post"
    ADD CONSTRAINT "sys_post_PKEY" PRIMARY KEY ("post_id");


--
-- Name: sys_role sys_role_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_role"
    ADD CONSTRAINT "sys_role_PKEY" PRIMARY KEY ("role_id");


--
-- Name: sys_role_dept sys_role_dept_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_role_dept"
    ADD CONSTRAINT "sys_role_dept_PKEY" PRIMARY KEY ("dept_id", "role_id");


--
-- Name: sys_role_menu sys_role_menu_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_role_menu"
    ADD CONSTRAINT "sys_role_menu_PKEY" PRIMARY KEY ("menu_id", "role_id");


--
-- Name: sys_user sys_user_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_user"
    ADD CONSTRAINT "sys_user_PKEY" PRIMARY KEY ("user_id");


--
-- Name: sys_user_online sys_user_online_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_user_online"
    ADD CONSTRAINT "sys_user_online_PKEY" PRIMARY KEY ("sessionId");


--
-- Name: sys_user_post sys_user_post_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_user_post"
    ADD CONSTRAINT "sys_user_post_PKEY" PRIMARY KEY ("post_id", "user_id");


--
-- Name: sys_user_role sys_user_role_PKEY; Type: CONSTRAINT; Schema: PUBLIC; Owner: SYSTEM
--

ALTER TABLE ONLY "PUBLIC"."sys_user_role"
    ADD CONSTRAINT "sys_user_role_PKEY" PRIMARY KEY ("role_id", "user_id");


--
-- Kingbase database dump complete
--

